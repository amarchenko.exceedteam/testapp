import React from 'react'
import adres from '../../Assets/icons/adress.svg'
import mail from '../../Assets/icons/mail.svg'
import phone from '../../Assets/icons/phone.svg'
import printer from '../../Assets/icons/printer.svg'
import './style.css'

const Footer = () => {
  return (
    <footer>
      <main className='footer-container'>
        <section className='input-footer'>
          <div className='footer-input-left'>
            <span className='footer-input-title'>
              Newsletter
            </span> 
            <span className='footer-input-subtitle'>
              Be the first one to know  about discounts, offers and events
            </span>
          </div> 
          <div className='footer-input-right'>
            <div className='back-input'>
              <span className='input-text-foot'>
                Enter your email
              </span>
              <button className='input-btn-fot'>
                Submit
              </button>
            </div>
          </div>
        </section>
        <section className='footer-describe'>
          <div className='foooter-logo-container'>
            <span className='footer-log'>
              Logo
            </span>
            <span className='footer-logo-desc'>
            We ara a lorem ipsum dolor sit amet, 
            consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore exercitation ullamco 
            laboris nisi ut aliquip ex ea commodo consequat... <span className='desc-read-more'>
                                                                   Read More
                                                                </span>
            </span>
          </div>
          <div className='footer-contact'>
            <div className='footer-contact-phone'>

            </div>
            <div className='footer-contact-mail'>

            </div>
            <div className='footer-contact-address'>
              
            </div>
            <div className='footer-contact-fax'>

            </div>
          </div>
        </section>
      </main>
    </footer>
  )
}

export default Footer
